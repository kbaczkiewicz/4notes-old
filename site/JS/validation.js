var password = document.getElementById('inputPassword');
var notif = document.getElementById('notif');
var logIn = document.getElementById('logIn');
					
function checkPassword()	{
	var pass = password.value;
	if(pass === ('') || pass.search("'") !== -1 || pass.search("\"")!== -1)	{
		notif.style.visibility = "visible";
		logIn.disabled = true;
							
	} else	{
		notif.style.visibility = "hidden";
		logIn.disabled = false;
	}
}
					
password.addEventListener('input',checkPassword , false);