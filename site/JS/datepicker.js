$(function () {
    $('#datetimepicker8').datetimepicker({
    locale: 'pl',
    format: 'YYYY-MM-DD',
    daysOfWeekDisabled: [0, 6],
    calendarWeeks: true
    });
});
