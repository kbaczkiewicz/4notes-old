function sort(kategoria) {
    switch(kategoria)    {
        case 1:
            var tryb = 2;
            var sortuj = 'data';
            var order = 'desc';
            break;
        case 2:
            var tryb = 2;
            var sortuj = 'przedmioty.nazwa';
            var order = 'asc';
            break;
        case 3:
            var tryb = 2;
            var sortuj = 'data';
            var order = 'asc';
            break;
        case 4:
            var tryb = 2;
            var sortuj = 'przedmioty.nazwa';
            var order = 'desc';
            break;
        default:
            break;
    }
        
   $.post('AJAX/sortuj.php', {posttryb:tryb, postsortuj:sortuj, postorder:order}, 
            function(data)  {
                $('#content').html(data);
            });
}
    
function szukaj()   {
    var lekcja = $('#szukajList').val();
    var data = $('#data').val();
        
    if(lekcja === "*wszystkie*") lekcja = "%";
    
        
    $.post('AJAX/szukaj.php', {postlekcja:lekcja, postdata:data},
            function(data)  {
               $('#content').html(data); 
            });
        
}
    
var szukajButton = document.getElementById('szukaj');
var sortDatyZA = document.getElementById('sortDatyZA');
var sortLekcjeAZ = document.getElementById('sortLekcjeAZ');
var sortDatyAZ = document.getElementById('sortDatyAZ');
var sortLekcjeZA= document.getElementById('sortLekcjeZA');

szukajList.addEventListener("change", szukaj, false);
sortDatyZA.addEventListener("click", function(){sort(1)}, false);
sortLekcjeAZ.addEventListener("click", function(){sort(2)}, false);
sortDatyAZ.addEventListener("click", function(){sort(3)}, false);
sortLekcjeZA.addEventListener("click", function(){sort(4)}, false);   
