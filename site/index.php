
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Logowanie do 4Notes</title>

		 <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
      
      <link href="signin.css" rel="stylesheet">
      
      <style>
                  body
                {
                    margin-bottom: 80px;
                    padding-top: 0;
                    background-color: #242424;
                }
      </style>
      
  </head>

  <body>
                         <div class="jumbotron" style="background-color:#333333;color:white;">
      <div class="container">
        <h1>4Notes</h1>
      </div>
    </div>  
    <div class="container">
              <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >

      <form class="form-signin" action="loguj.php" method="POST">

        <label for="inputLogin" class="sr-only">Login</label>
          
        <input type="text" id="inputLogin" name="login" class="form-control" placeholder="Login" pattern="[a-zA-Z0-9_]{1,}" autofocus required>
        <script>
          var input = document.getElementById('inputLogin');
            
            input.oninvalid = function(event) {
    event.target.setCustomValidity('Login zawiera niedozwolone znaki');
}
          </script>
          
        <label for="inputPassword" class="sr-only">Hasło</label>
          
        <input type="password" id="inputPassword" name="haslo" class="form-control" placeholder="Hasło" required>
          
          <div class="checkbox" style="color:white;">
    <label>
      <input type="checkbox" required> Logując się akceptuję <a href="./aplikacja/regulamin.PDF">regulamin</a>
    </label>
  </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj się</button>
          <script type='text/javascript' src='JS/validation.js'></script>
      </form>     
       <a href='Nindex.php'>Logowanie dla nauczycieli</a> 
    </div> <!-- /container -->
</div>
        </div>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
