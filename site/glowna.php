<?php
    ob_start();
    session_start();

    

    require_once('requires.php');		
        
    $User = new User();
        
    try	{
        $User->LczyZalog();
        $User->LsprawdzToken();
        
    } catch(Exception $e)	{
        echo "Error!".$e->getMessage();
        echo "<br /><a href='index.php'>Wróć</a>";
        die();
    }
    
        $User->Cwyczysc();
	$przedmioty = $User->CwybierzPrzedmioty();
	$User->Cwybierz();
	
?>
<!DOCTYPE html>
<html>
    <head>
        <title>4Notes</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta charset='utf-8'>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment-with-locales.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

            <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
							
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
            <style>
            body
                {
                    margin-bottom: 80px;
                    background-color:#242424;
                    
                }
                
                .test
                {
                    width:100%;
                }
            </style>
    </head>
    <body>
            
            
               
	<nav class='navbar navbar-default' style="background-color:white;margin-bottom:0;color:#242424;">
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <a class='navbar-brand' href='glowna.php'>Strona główna</a>
		</div>
                <div>
                <ul class="nav navbar-nav navbar-right">
                    <li class='dropdown'>
                        <a class='dropdown-toggle' data-toggle='dropdown' href='#'>Menu użytkownika
                            <span class='glyphicon glyphicon-user'></span></a>
                        <ul class="dropdown-menu">
                            <li><a href='http://planlekcji.zs6sobieski.pl' target='_blank'>Plan lekcji</a></li>
                            <li><a href='profil.php'>Profil</a></li>
                            <li><a href='wyloguj.php'>Wyloguj</a></li>
                        </ul>
		</div>
            </div>
	</nav>
        <div class="jumbotron" style="background-color:#333333;color:white;">
            <div class="container">
                <h1>4Notes</h1>
            </div>
        </div>
               
        <div class = "container">
            <div class = "row">
                <div class = "col-lg-9" style="background-color:#333333;border-radius:5px;padding:15px 15px 15px 15px;" id='content'>
                <?php
                    $User->Cwyswietl();
                ?>
                    
                </div>
                            
                            
                <div class = "col-lg-3" style="">
                    <div class="form-group">
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker8' name='data'>	<!-- DATA DATA DATA -->
				<input type='text' class="form-control" id='data' />
				<span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar">
                                    </span>
				</span>
                            </div>
			</div>
                            
                        <script type='text/javascript' src='JS/datepicker.js'></script>
                            
                        <select class="selectpicker show-menu-arrow" title="Przedmiot" data-width="100%" id='szukajList'><!-- PRZEDMIOTY PRZEDMIOTY PRZEDMIOTY-->
                            <optgroup label="">
                                <option>*wszystkie*</option>
                            </optgroup>

                            <optgroup label="">
                                <?php for($a = 0;$a<count($przedmioty);$a++)    {echo "<option>".$przedmioty[$a]."</option>";} ?>
                            </optgroup>
                        </select>
                            
                        <div class = "panel panel-primary" style="margin-top:20px;">
                            <div class = "panel-heading"><h3 class="panel-title">Sortowanie</h3></div>

                            <table class = "table">     
                                <tr>
                                    <td>Przedmiot</td>
                                    <td>
                                        <button class="btn btn-default glyphicon glyphicon-sort-by-alphabet" type="button" id='sortLekcjeAZ'></button>
                                        <button class="btn btn-default glyphicon glyphicon-sort-by-alphabet-alt" type="button" id='sortLekcjeZA'></button>
                                    </td>
                                    
                                </tr>

                                <tr>
                                    <td>Data</td>
                                    <td>
                                        <button class="btn btn-default glyphicon glyphicon-sort-by-order" type="button" id='sortDatyZA'></button>
                                        <button class="btn btn-default glyphicon glyphicon-sort-by-order-alt" type="button" id='sortDatyAZ'></button>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            
            <div class = "navbar navbar-default navbar-fixed-bottom" style="min-height:5px;">
               
                <div class = "container" style="margin:0px;padding:0px;">
                    <p class = "navbar-text pull-left" style="font-size:x-small;margin-top:5px;margin-bottom:5px;">Projekt stworzony przez Korneliusza Buczkowskiego oraz Kamila Bączkiewicza</p>
                </div>
               
            </div>
        </div>
    <script type='text/javascript' src='JS/PHPPost.js'></script>
    
    </body>
</html>

<?php   ob_end_flush();?>