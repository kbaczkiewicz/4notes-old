<?php
ob_start();
session_start();	
        
        require_once('requires.php');
	
        $User = new User();
	
	try	{
	$User->LczyZalog();
        $User->LczyZalog();
	} catch(Exception $e)	{
		echo "Error!".$e->getMessage();
                echo "<br /><a href='index.php'>Wróć</a>";
                die();
	}
	
?>

<!DOCTYPE html>
<html>
    <head>
        <title>4Notes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment-with-locales.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
            
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

        <style>
            body
                {
                    margin-bottom: 80px;
                    background-color: #242424;
                }
                
                .test
                {
                    width:100%;
                }
                .user-row {
                    margin-bottom: 14px;
                }

                .user-row:last-child {
                    margin-bottom: 0;
                }

                .dropdown-user {
                    margin: 13px 0;
                    padding: 5px;
                    height: 100%;
                }

                .dropdown-user:hover {
                    cursor: pointer;
                }

                .table-user-information > tbody > tr {
                    border-top: 1px solid rgb(221, 221, 221);
                }

                .table-user-information > tbody > tr:first-child {
                    border-top: 0;
                }


                .table-user-information > tbody > tr > td {
                    border-top: 0;
                }
                .toppad
                {margin-top:20px;
                }

        </style>
    </head>
    <body>
	<nav class='navbar navbar-default' style="background-color:white;margin-bottom:0;color:#242424;">
            <div class='container-fluid'>
		<div class='navbar-header'>
                    <a class='navbar-brand' href='glowna.php'>Strona główna</a>
                </div>
		<div>
                    <ul class="nav navbar-nav navbar-right">
			<li class='dropdown'>
                            <a class='dropdown-toggle' data-toggle='dropdown' href='#'>Menu użytkownika
				<span class='glyphicon glyphicon-user'></span></a>
                                    <ul class="dropdown-menu">
					<li><a href='http://planlekcji.zs6sobieski.pl' target='_blank'>Plan lekcji</a></li>
					<li><a href='profil.php'>Profil</a></li>
					<li><a href='wyloguj.php'>Wyloguj</a></li>
                                    </ul>				
		</div>
            </div>
	</nav>
        
        <div class="jumbotron" style="background-color:#333333;color:white;">
            <div class="container">
                <h1>4Notes</h1>
            </div>
        </div>  
        
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <?php
                    $User->UpokazProfil();
                ?>
                    
                </div>
            </div>
        </div>
        
        <div class = "navbar navbar-default navbar-fixed-bottom" style="min-height:5px;">
            <div class = "container" style="margin:0px;padding:0px;">
                <p class = "navbar-text pull-left" style="font-size:x-small;margin-top:5px;margin-bottom:5px;">Projekt stworzony przez Korneliusz Buczkowski oraz Kamil Bączkiewicz</p>
            </div>
               
        </div>
        
        <div class = "modal fade" id = "zmienHaslo" role = "dialog">
            <div class = "modal-dialog">
                <div class = "modal-content">
                    <div class = "modal-header">
                        <h4>Zmień hasło</h4>
                    </div>
                    <div class = "modal-body">
                        <form action="zmianahasla.php" method="post">
                            <div class="form-group">
                                <label for="stPass">Stare hasło</label>
                                    <input type="password" class="form-control" id="stPass" name="sthaslo" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="nPass1">Nowe hasło</label>
                                    <input type="password" id="nPass1" name="nhaslo1" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="nPass2">Powtórz nowe hasło</label>
                                    <input type="password" id="nPass2" name="nhaslo2" class="form-control" placeholder="">
                            </div>
                            <input type='submit' class='btn btn-primary' value='Zmień hasło'>
                        </form>
                    </div>
                    <div class = "modal-footer">   
                            
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
ob_end_flush();
?>