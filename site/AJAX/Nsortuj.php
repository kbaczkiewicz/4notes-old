<?php

   session_start();

   require_once('../requires.php');
    
   $NUser = new NUser();
   
   try	{
        $NUser->LczyZalog();
        $NUser->LsprawdzToken();
   } catch(Exception $e)	{
        echo "Error!".$e->getMessage();
        echo "<br /><a href='index.php'>Wróć</a>";
        die();
	}

   $posttryb = $_POST['posttryb'];
   $postsortuj = $_POST['postsortuj'];
   $postorder = $_POST['postorder'];
   
   
   $NUser->Cwybierz($posttryb, $postsortuj, $postorder);
   $NUser->Cwyswietl();
    
