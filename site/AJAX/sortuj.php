<?php

   session_start();

   require_once('../requires.php');
    
   $User = new User();
   
   try	{
        $User->LczyZalog();
        $User->LsprawdzToken();
   } catch(Exception $e)	{
        echo "Error!".$e->getMessage();
        echo "<br /><a href='index.php'>Wróć</a>";
        die();
	}

   $posttryb = $_POST['posttryb'];
   $postsortuj = $_POST['postsortuj'];
   $postorder = $_POST['postorder'];
   
   
   $User->Cwybierz($posttryb, $postsortuj, $postorder);
   $User->Cwyswietl();
    
