<?php
	class Baza  {
		
		protected $m_BD;
		
		protected function Bpolacz()	{
			try {  
                                        $pdo = new PDO('mysql:host=localhost;dbname=00440922_notatki', 'root', '');  
                                        $pdo->exec ('SET NAMES utf8');
					$pdo->exec('SET CHARACTER_SET utf8_unicode_ci');
				} catch (PDOException $e)   {
					print "Error!: " .$e->getMessage() ."<br />";
					die();
				}
					$this->m_BD = $pdo;			
		}
		
		protected function Bwykonaj($zapytanie)	{
			try	{
				$this->m_BD->exec($zapytanie);
			}	catch (Exception $e)	{
				echo "Error!".$e->getMessage();
			}
		}
		
		protected function Bwybierz($zapytanie)	{
			if(!$this->m_BD->query($zapytanie))	{
				throw new Exception('Zapytanie nie zostało wykonane!');
			}
			return $this->m_BD->query($zapytanie);
		}
		
		public function BzacznijSesje()	{
			session_start();
			
			$_SESSION['_PRAWLOGIN'] = '';				//zmienna logowania - NIE DO RUSZENIA
			$_SESSION['_KLASA'] = '';                                //zmienna logowania - NIE DO RUSZENIA							
			$_SESSION['_SORTUJ_LEKCJA'] = '';			//zmienna 'neutralizowana' przy kazdym wejsciu na glowna
			$_SESSION['_SORTUJ_DATA'] = '';			//zmienna 'neutralizowana' przy kazdym wejsciu na glowna	
			$_SESSION['_NOTATKA'] = '';
                        $_SESSION['_PRZYW'] = '';                                //zmienna logowania - NIE DO RUSZENIA
                        $_SESSION['_IDKL'] = '';                                //zmienna logowania - NIE DO RUSZENIA
			$_SESSION['_PRZEDMIOTY'] = '';                           //przedmioty do wyboru
			
		}
                
                public function BNzacznijSesje()	{
			session_start();
			
                        $_SESSION['_PRAWLOGIN'] = '';
                        $_SESSION['_PRZEDMIOT'] = '';
                        $_SESSION['_IDPRZ'] = '';
                        $_SESSION['_SORTUJ_LEKCJA'] = '';
                        $_SESSION['_SORTUJ_DATA'] = '';
                        $_SESSION['_NOTATKA'] = '';
                        $_SESSION['_NOTATKI'] = '';
			
		}
	};
?>