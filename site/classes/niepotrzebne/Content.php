<?php

	class Content extends Baza implements Notes 	{
		protected $m_kwerenda;
		protected $m_notatki;
		protected $m_notatka;
		protected $m_przedmioty;
		protected $m_lekcja;
		protected $m_data;
		protected $m_idKlasy;
		protected $m_klasa;
                protected $m_przywilej;
		
                //uczniowska, rozni sie od nauczycielskiej dlatego jest poza interfejsem
		public function Cwybierz($tryb = 0, $sortuj='data', $order='desc', $lekcja = '', $data = '')	{
			$this->m_idKlasy = $_SESSION['_IDKL'];
			$this->m_notatka = array('id' => '', 'lekcja' => '', 'temat' => '', 'data' => '');
                        $indeks = 0;
			
			switch($tryb)	{
				case 0:         //na glownej, bez zadnej flagi																										//od razu po zalogowaniu - 5 ostatnich notatek								
					$this->m_kwerenda = "select notatki.ID, przedmioty.nazwa,temat, DATA from notatki inner join
                                        przedmioty on notatki.ID_lekcji = przedmioty.ID where ID_klasy = ".$this->m_idKlasy."
                                        order by notatki.".$sortuj." ".$order.";";
					break;
				case 1:         //flaga "wyszukiwanie"
					//$this->m_lekcja = $_POST['lekcja'];
					//$this->m_data = $_POST['data'];
                                        $this->m_lekcja = $lekcja;
                                        $this->m_data = $data;
				
					if($this->m_lekcja == "*wszystkie*")	{$this->m_lekcja="%";}
					if($this->m_data == "")	{$this->m_data="%";}
					
					if(!empty($this->m_lekcja) && !empty($this->m_data))	{
                                            $this->m_kwerenda = "select notatki.ID, przedmioty.nazwa,notatki.temat, notatki.DATA from notatki 
                                            inner join przedmioty on notatki.ID_lekcji = przedmioty.ID where przedmioty.nazwa like '".$this->m_lekcja."'
                                            and notatki.DATA like '".$this->m_data ."' and ID_klasy = ".$this->m_idKlasy."
                                            order by data desc;";
                                            
                                            $_SESSION['_SORTUJ_LEKCJA'] = $this->m_lekcja;
                                            $_SESSION['_SORTUJ_DATA'] = $this->m_data;
					}
					break;
				case 2:             //flaga "sortowanie"
					$this->m_lekcja = $_SESSION['_SORTUJ_LEKCJA'];
					$this->m_data = $_SESSION['_SORTUJ_DATA'];
					
					//$sortuj = $_GET['sortuj'];
					//$order = $_GET['order'];
					
					if(!empty($this->m_lekcja) && !empty($this->m_data))	{
                                            $this->m_kwerenda = "select notatki.ID, przedmioty.nazwa,notatki.temat, notatki.DATA from notatki
                                            inner join przedmioty on notatki.ID_lekcji = przedmioty.ID where przedmioty.nazwa like '".$this->m_lekcja."'
                                            and data like '".$this->m_data ."' and ID_klasy = ".$this->m_idKlasy." order by ".$sortuj.";";
					}   else	{
                                            $this->m_kwerenda = "select notatki.ID, przedmioty.nazwa,temat, DATA from notatki
                                            inner join przedmioty on notatki.ID_lekcji = przedmioty.ID where ID_klasy = 
                                            ".$this->m_idKlasy." order by ".$sortuj." ".$order.";";
					}
					break;
				default:
                                        $this->m_kwerenda = "select notatki.ID, przedmioty.nazwa,temat, DATA from notatki inner join
                                        przedmioty on notatki.ID_lekcji = przedmioty.ID where ID_klasy ='".$this->m_idKlasy."'
                                        order by ".$sortuj." ".$order." limit 5;";
                                        break;
						
				
			}
			
			$this->Bpolacz();
			
			foreach($this->m_BD->query($this->m_kwerenda) as $wynik)	{
                            $this->m_notatki['id'][$indeks] = $wynik[0];
                            $this->m_notatki['lekcja'][$indeks] = $wynik[1];
                            $this->m_notatki['temat'][$indeks] = $wynik[2];
                            $this->m_notatki['data'][$indeks] = $wynik[3];
							$indeks++;
			}
			
			$_SESSION['_NOTATKI'] = $this->m_notatki;
		}
		
		public function Cwyswietl()	{                     
             
                        if(!$this->m_notatki['temat'])
                            {
                                echo "<a href='#' style='text-decoration:none;'>
                                      <div class = 'panel panel-primary'>
                                      <div class = 'panel-heading'>
                                      <h3 class = 'panel-title'>	<!-- LEKCJA LEKCJA LEKCJA -->
                                      <p>Brak notatek!</p><br>
				      </h3>
				      </div>
	   
				      <div class = 'panel-body'><!-- TEMAT TEMAT TEMAT -->
                                      Nie ma żadnych notatek z tego przedmiotu w tym dniu.
				      </div>
				      </div>
				      </a>";
                            }
			for($a = 0;$a<sizeof($this->m_notatki['temat']);$a++)	{
                                echo "<a href='notatka.php?id=".$this->m_notatki['id'][$a]."' style='text-decoration:none;'>
                                      <div class = 'panel panel-primary'>
                                      <div class = 'panel-heading'>
                                      <h3 class = 'panel-title'>	<!-- LEKCJA LEKCJA LEKCJA -->
                                      ".$this->m_notatki['lekcja'][$a]."<br>
				      <small>".$this->m_notatki['data'][$a]."</small>
				      </h3>
				      </div>
	   
				      <div class = 'panel-body'><!-- TEMAT TEMAT TEMAT -->"
				      .$this->m_notatki['temat'][$a]."
				      </div>
				      </div>
				      </a>";
			}
                        
		}
		
		public function Cwyczysc()	{
			$_SESSION['_SORTUJ_LEKCJA'] = '';
			$_SESSION['_SORTUJ_DATA'] = '';			
		}
		
		public function CwybierzNotatke()	{
                    $id = $_GET['id'];
                    $this->m_idKlasy = $_SESSION['_IDKL'];

                    $this->m_kwerenda = "select notatki.id, notatki.temat, notatki.data, notatki.tresc, "
                            . "przedmioty.nazwa from notatki join przedmioty on notatki.ID_lekcji = przedmioty.ID"
                            . " where notatki.ID='".$id."' and notatki.ID_klasy = ".$this->m_idKlasy.";";

                    $this->m_notatka = array('id' => '', 'temat' => '', 'tresc' => '', 'data' => '', 'lekcja' => '');

                    $this->Bpolacz();

                    foreach($this->m_BD->query($this->m_kwerenda) as $wynik)   {
                        $this->m_notatka['id'] = $wynik[0];
                        $this->m_notatka['temat'] = $wynik[1];
                        $this->m_notatka['data'] = $wynik[2];
                        $this->m_notatka['tresc'] = $wynik[3];
                        $this->m_notatka['lekcja'] = $wynik[4];
                    }
                                      
                    $_SESSION['_NOTATKA'] = $this->m_notatka;
                                           	
		}
		
		public function CwyswietlNotatke()	{
			echo "<h3 class='panel-title'>".
                              $this->m_notatka['temat']."<br>      
                              <small>".$this->m_notatka['lekcja']."<br>
                              ".$this->m_notatka['data']."
                              </small>
                              </h3>
                              </div>
                              <div class = 'panel-body' id='note'>
                              <p>".$this->m_notatka['tresc']."</p>
                              </div>
                              </div>
                              </div>";
		}
		
		public function CwybierzPrzedmioty()	{
                    $this->m_idKlasy = $_SESSION['_IDKL'];
                    $this->m_przedmioty = array();
                    $i = 0;

                    $this->Bpolacz();

                    $wynik = $this->m_BD->query("select przedmioty.nazwa from przedmioty inner join nauczyciele_przedmiotow on przedmioty.ID = nauczyciele_przedmiotow.przedmiot"
                            . " where nauczyciele_przedmiotow.klasa = '".$this->m_idKlasy."';");

                    foreach($wynik as $value) {
                        $this->m_przedmioty[$i] = $value[0];
                        $i++;
                    }

                    $_SESSION['_PRZEDMIOTY'] = $this->m_przedmioty;
                    return $this->m_przedmioty;
                                        
			
                }
		
		public function Cedytuj($notatka)	{
                    $this->m_notatka = $_SESSION['_NOTATKA'];   
                    $tresc = @ strip_tags($notatka, "<FONT><P><B><td><blockquote><ul><div><p><li><h1><table><hr><span><b><font><a><img>");

                    $this->m_przywilej = $_SESSION['_PRZYW'];

                    $this->Bpolacz();

                    if($this->m_przywilej > 1)    {
                        $this->m_BD->exec("update notatki set tresc='".$tresc."' where id='".$this->m_notatka['id']."';");
                    } else    {
                        throw new Exception('Niewystarczające przywileje!');
                    }                    
                    return true;
                }
                
                
               /* public function CwyswietlEdyt() {
                    $this->m_notatka = $_SESSION['_NOTATKA'];
                    
                    echo "<div class='col-md-6 col-md-offset-3'>
                          <form action='edycja.php' method='post'>
                          <div class='form-group'>
                          <label for='tresc'>Treść</label>
                          <textarea class='form-control' name='tresc' rows='25'>"
                          .$this->m_notatka['tresc'] 
                          ."</textarea>
                          </div>
                          <button type='submit' class='btn btn-default'>Edytuj</button>
                          </form>
                          </div>
                          </div>";
                    
                }*/
                
                /*public function NodswiezNotatki()   {
                    if(!$this->m_notatki['temat'])  {
                                echo "<a href='#'  style='text-decoration:none;'>
                                      <div class = 'panel panel-primary'>
                                      <div class = 'panel-heading'>
                                      <h3 class = 'panel-title'>	<!-- LEKCJA LEKCJA LEKCJA -->
                                      Brak notatek!<br>
				      </h3>
				      </div>
	   
				      <div class = 'panel-body'><!-- TEMAT TEMAT TEMAT -->
                                      Nie ma żadnych notatek z tego przedmiotu w tym dniu.
				      </div>
				      </div>
				      </a>";
                                return;
                    }
                    
                    for($a = 0;$a<sizeof($this->m_notatki['temat']);$a++)	{
                                echo "<a href='#'  style='text-decoration:none;'>
                                      <div class = 'panel panel-primary'>
                                      <div class = 'panel-heading'>
                                      <h3 class = 'panel-title'>	<!-- LEKCJA LEKCJA LEKCJA -->"
                                      .$this->m_notatki['lekcja'][$a]."<br>
				      <small>".$this->m_notatki['data'][$a]."</small>
				      </h3>
				      </div>
	   
				      <div class = 'panel-body'><!-- TEMAT TEMAT TEMAT -->"
				      .$this->m_notatki['temat'][$a]."
				      </div>
				      </div>
				      </a>";
                    }
                }*/
    
        };
?>