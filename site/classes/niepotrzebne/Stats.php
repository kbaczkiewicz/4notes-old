<?php
    
    class Stats extends Baza   {
        private $m_plik;
        private $m_tresc;
        private $m_data;
        
        public function SzapiszLog()  {
            $this->m_plik = fopen("logi/logowanie.log", "a");
            $this->m_tresc = "******\nLogin:".$_SESSION['_PRAWLOGIN']."\nData: ".date("F j, Y, g:i a")."\nIP:".$_SERVER['REMOTE_ADDR']."\n******\n";    
            fwrite($this->m_plik, $this->m_tresc);
            fclose($this->m_plik);
        }
        
        public function SzapiszEdyt()  {
            $this->m_plik = fopen("logi/edycja.log", "a");
            $this->m_tresc = "******\nLogin:".$_SESSION['praw_login']."\nData: ".date("F j, Y, g:i a")."\nNotatka:".$_SESSION['_NOTATKA']['temat']."\n******\n";
            fwrite($this->m_plik, $this->m_tresc);
            fclose($this->m_plik);
            
        }
        
        public function SdodajLicznik() {
            if($_COOKIE['licznik']!="1")   {
                $this->Bpolacz();
                $this->m_data = date('Y-m-d');
                if($this->m_data == $this->SsprawdzLicznik())   {
                    $this->m_zapytanie = "update counter set counter = counter + 1 where data = '".$this->m_data."';";
                    $this->Bwykonaj($this->m_zapytanie);
                    
                }   else    {
                    $this->m_zapytanie = "insert into counter(data,counter) values ('".$this->m_data."',1);";
                    $this->Bwykonaj($this->m_zapytanie);
                }
                setcookie('licznik', 1);
            }   else    {
                $this->m_plik = fopen("logi/cookie.log", "a");
                $this->m_tresc = 'The cake is a lie!';
                fwrite($this->m_plik, $this->m_tresc);
            }
            setcookie('licznik','1');
                
        }
        
        public function SsprawdzLicznik()   {
            $this->Bpolacz();
            $this->m_zapytanie = "select data from counter order by data desc limit 0,1";
            foreach($this->Bwybierz($this->m_zapytanie) as $value)  {
                $data = $value[0];
                $this->m_plik = fopen("logi/data.log", "a");
                $this->m_tresc = $data;
                fwrite($this->m_plik, $this->m_tresc);
            }
            return $data;
        }
       
    };