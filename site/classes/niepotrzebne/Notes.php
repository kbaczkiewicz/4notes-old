<?php

    interface Notes   {
        //public function Cwybierz($tryb = 0, $sortuj='data', $order='desc');
        public function Cwyswietl();
        public function Cwyczysc();
        public function CwybierzNotatke();
        public function CwyswietlNotatke();
        public function CwybierzPrzedmioty();
        public function Cedytuj($notatka);
        
}