<?php

	class User extends Content  implements Loguj	{
		private $m_dane;
		private $m_zapytanie;
                private $m_token;
                private $m_imie;
                private $m_nazwisko;
		
		public function Lloguj($login, $haslo)	{
                    
                        if(@ ereg("['\";]", $login))   {
                            throw new Exception('Login zawiera nieprawidłowe znaki!');
                        }
				
			$this->Bpolacz();
				
			$this->m_zapytanie = "select users.login,klasy.nazwa, users.przywilej, users.ID_klasy, users.imie, users.nazwisko from users inner join klasy
										   on users.ID_klasy = klasy.ID
									       where users.login='" .$login ."' and users.haslo=sha1('" .$haslo."');";
				
			try	{
				$this->m_dane = $this->Bwybierz($this->m_zapytanie);
			} catch (Exception $e)	{
				echo "Error!".$e->getMessage();
				die();
			}
				
			if($this->m_dane->rowCount() < 1)	{
				throw new Exception('Login/hasło błędne!');
			}
				
			foreach($this->m_dane as $value)   {
				$this->m_klasa = $value[1];
                        $this->m_przywilej = $value[2];
                        $this->m_idKlasy = $value[3];
                        $this->m_imie = $value[4];
                        $this->m_nazwisko = $value[5];
			}
					
			$this->BzacznijSesje();
			$_SESSION['_PRAWLOGIN'] = $login;
			$_SESSION['_KLASA'] = $this->m_klasa;
			$_SESSION['_PRZYW'] = $this->m_przywilej;
			$_SESSION['_IDKL'] = $this->m_idKlasy;
                        $_SESSION['_IMIE'] = $this->m_imie;
                        $_SESSION['_NAZWISKO'] = $this->m_nazwisko;
			$_SESSION['_SORTUJ_LEKCJA'] = '';
			$_SESSION['_SORTUJ_DATA'] = '';
			$_SESSION['_NOTATKA'] = '';
			$_SESSION['_NOTATKI'] = '';
				
			return true;
           
		}
		
		public function Lwyloguj()	{
			if(isset($_SESSION['_PRAWLOGIN']))    {
                session_destroy();?>
                    <script language="javascript"> 
                    window.location = "index.php";   
                    </script><?php
			}
			
		}
                
                public function LwyslijToken()  {
                    $login = $_SESSION['_PRAWLOGIN'];
                    
                    $this->m_token = sha1(time());
                    $this->m_zapytanie = "update users set token='".$this->m_token."' where login='".$login."';";
                    $this->Bwykonaj($this->m_zapytanie);
                    setcookie('token',$this->m_token);
                }
                
                public function LsprawdzToken() {
                    
                    $login = $_SESSION['_PRAWLOGIN'];
                    
                    $this->Bpolacz();
                    $this->m_zapytanie = "select token from users where login='".$login."';";
                    $wynik = $this->Bwybierz($this->m_zapytanie);
                    
                    foreach($wynik as $value)  {
                        $this->m_token = $value[0];
                    }
                    
                    if($this->m_token != $_COOKIE['token']) {
                        throw new Exception('Wykryto logowanie z innego urządzenia! Zaloguj się ponownie');
                    }
                    return true;
                }
		
		public function LzmienHaslo($stHaslo, $nHaslo1, $nHaslo2)	{
			
			$login = $_SESSION['_PRAWLOGIN'];
			
			if(empty($nHaslo1) || empty($nHaslo2) || empty($stHaslo)) {
				throw new Exception ('Źle wypełniony formularz!');
            } elseif ($nHaslo1 !== $nHaslo2) {
                throw new Exception('Podane hasła różnią się od siebie!');
            }
			
			$nHaslo = $nHaslo1;
								
										//sprawdzanie czy uzytkownik istnieje
			
			$this->m_zapytanie = "select haslo from users where login='".$login."' and haslo=sha1('".$stHaslo."');";
			
			$this->Bpolacz();
			
			$this->m_dane = $this->Bwybierz($this->m_zapytanie);
			
			if($this->m_dane->rowCount() < 1)  {
                throw new Exception('Podane aktualne hasło jest nieprawidłowe!');
            }
			
										//wlasciwa zmiana hasla
			
			$this->m_kwerenda = "update users set haslo=sha1('".$nHaslo."')  where login='".$login."' and haslo=sha1('".$stHaslo."');";
			
			try	{
					$this->Bwykonaj($this->m_kwerenda);
			}	catch(Exception $e)	{
				echo "Error!".$e->getMessage();
				echo "<br /><a href='glowna.php'>Wróć</a>";
				die();
			}
			
			session_destroy();
                                        
                ?>
                    <script language="javascript"> 
                    window.location = "index.php";   
                    </script><?php				
		}
		
		public function LczyZalog()	{
			if(isset($_SESSION['_PRAWLOGIN']))
			{
				return true;
			}   else    {
				throw new Exception('Jesteś niezalogowany!');
			}
			
		}
     
                
                public function UsprawdzPrzyw() {
                    $this->m_przywilej = $_SESSION['_PRZYW'];
                    
                    if($this->m_przywilej <2)   {
                        throw new Exception("Niewystarczający przywilej!");
                    }
                    
                    return true;
                }
                
                public function UpokazProfil()  {
                    $this->m_login = $_SESSION['_PRAWLOGIN'];
                    $this->m_klasa = $_SESSION['_KLASA'];
                    $this->m_imie = $_SESSION['_IMIE'];
                    $this->m_nazwisko = $_SESSION['_NAZWISKO'];
                    $this->m_przywilej = ($_SESSION['_PRZYW'] == 1) ? "Przeglądanie" : "Przeglądanie i edycja";
                    
                    echo "<div class='panel panel-primary'>
                          <div class='panel-heading'>
                          <h3 class='panel-title'>".$this->m_imie." ".$this->m_nazwisko."</h3>
                          </div>
                          <div class='panel-body'>
                          <div class='row'>
                          <div class='col-md-3 col-lg-3 ' align='center'> <img alt='User Pic' src='http://cloudpath.net/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png' class='img-circle img-responsive'> </div>
                          <div class=' col-md-9 col-lg-9 '> 
                          <table class='table table-user-information'>
                          <tbody>
                          <tr>
                          <td>Login:</td>
                          <td>".$this->m_login."</td>
                          </tr>
                          <tr>
                          <td>Klasa:</td>
                          <td>".$this->m_klasa."</td>
                          </tr>
                          <tr>
                          <td>Przywilej</td>
                          <td>".$this->m_przywilej."</td>
                          </tr>
                          </tbody>
                          </table>
                          </tbody>
                          </table>
                          <a class='btn btn-default' data-toggle='modal' data-target='#zmienHaslo'>Zmień hasło</a>
                          <a href='wyloguj.php' class='btn btn-default'>Wyloguj się</a>
                          </div>
                          </div>
                          </div>         
                          </div>";
                }
		
        };
?>