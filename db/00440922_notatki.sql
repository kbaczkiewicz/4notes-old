-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 26 Maj 2016, 12:55
-- Wersja serwera: 10.1.9-MariaDB
-- Wersja PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `00440922_notatki`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `counter`
--

CREATE TABLE `counter` (
  `data` date NOT NULL DEFAULT '0000-00-00',
  `counter` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Licznik odwiedzin';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasy`
--

CREATE TABLE `klasy` (
  `ID` int(11) NOT NULL,
  `nazwa` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `nauczyciele`
--

CREATE TABLE `nauczyciele` (
  `ID` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `haslo` varchar(50) NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(50) NOT NULL,
  `przedmiot` int(11) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `nauczyciele_przedmiotow`
--

CREATE TABLE `nauczyciele_przedmiotow` (
  `ID` int(11) NOT NULL,
  `klasa` int(11) DEFAULT NULL,
  `przedmiot` int(11) DEFAULT NULL,
  `nauczyciel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `notatki`
--

CREATE TABLE `notatki` (
  `ID` int(11) NOT NULL,
  `ID_lekcji` int(11) DEFAULT NULL,
  `ID_klasy` int(11) DEFAULT NULL,
  `tresc` text COLLATE utf8_unicode_ci,
  `data` date DEFAULT NULL,
  `temat` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmioty`
--

CREATE TABLE `przedmioty` (
  `ID` int(11) NOT NULL,
  `nazwa` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `login` varchar(16) NOT NULL,
  `haslo` varchar(40) NOT NULL,
  `imie` varchar(30) CHARACTER SET utf8 NOT NULL,
  `nazwisko` varchar(50) CHARACTER SET utf8 NOT NULL,
  `przywilej` int(11) NOT NULL,
  `ID_klasy` int(11) DEFAULT NULL,
  `token` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `counter`
--
ALTER TABLE `counter`
  ADD PRIMARY KEY (`data`);

--
-- Indexes for table `klasy`
--
ALTER TABLE `klasy`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `nauczyciele`
--
ALTER TABLE `nauczyciele`
  ADD PRIMARY KEY (`ID`,`login`);

--
-- Indexes for table `nauczyciele_przedmiotow`
--
ALTER TABLE `nauczyciele_przedmiotow`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `klasa` (`klasa`),
  ADD KEY `przedmiot` (`przedmiot`),
  ADD KEY `nauczyciel` (`nauczyciel`);

--
-- Indexes for table `notatki`
--
ALTER TABLE `notatki`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_lekcji` (`ID_lekcji`),
  ADD KEY `ID_klasy` (`ID_klasy`);

--
-- Indexes for table `przedmioty`
--
ALTER TABLE `przedmioty`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_klasy` (`ID_klasy`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `klasy`
--
ALTER TABLE `klasy`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `nauczyciele_przedmiotow`
--
ALTER TABLE `nauczyciele_przedmiotow`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=896;
--
-- AUTO_INCREMENT dla tabeli `notatki`
--
ALTER TABLE `notatki`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=342;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `nauczyciele_przedmiotow`
--
ALTER TABLE `nauczyciele_przedmiotow`
  ADD CONSTRAINT `nauczyciele_przedmiotow_ibfk_1` FOREIGN KEY (`klasa`) REFERENCES `klasy` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `nauczyciele_przedmiotow_ibfk_2` FOREIGN KEY (`przedmiot`) REFERENCES `przedmioty` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `nauczyciele_przedmiotow_ibfk_3` FOREIGN KEY (`nauczyciel`) REFERENCES `nauczyciele` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `notatki`
--
ALTER TABLE `notatki`
  ADD CONSTRAINT `notatki_ibfk_1` FOREIGN KEY (`ID_lekcji`) REFERENCES `przedmioty` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `notatki_ibfk_2` FOREIGN KEY (`ID_klasy`) REFERENCES `klasy` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ID_klasy`) REFERENCES `klasy` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
